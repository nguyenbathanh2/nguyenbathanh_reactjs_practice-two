// Libraries
import PropTypes from 'prop-types';
import React from 'react';
import './input.module.css';

const Input = ({
  minValue,
  label,
  onChange,
  value,
  type,
  classNameLabel,
  className,
  placeholder,
  refInput,
}) => {
  return (
    <label className={classNameLabel}>
      {label}
      <input
        min={minValue}
        ref={refInput}
        onChange={onChange}
        value={value}
        className={className}
        placeholder={placeholder}
        type={type}
      />
    </label>
  );
};

Input.propTypes = {
  refInput: PropTypes.object,
  label: PropTypes.string,
  classNameLabel: PropTypes.string,
  minValue: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string.isRequired,
};

Input.defaultProps = {
  refInput: null,
  label: null,
  classNameLabel: null,
  value: '',
  minValue: null,
  placeholder: null,
};

export default Input;
