import Input from './Input';
import styles from './input.module.css';

export default {
  title: 'Components/Input',
  component: Input,
};

function Template(args) {
  return <Input {...args} />;
}

export const InputTitle = Template.bind({});
InputTitle.args = {
  onChange: () => {},
  className: styles.input_items,
  placeholder: 'title',
  value: '',
  type: '',
};

export const InputPrice = Template.bind({});
InputPrice.args = {
  onChange: () => {},
  className: styles.input_items,
  placeholder: 'price',
  value: '',
  type: '',
};

export const InputImage = Template.bind({});
InputImage.args = {
  onChange: () => {},
  className: styles.input_items,
  placeholder: 'URL Image',
  value: '',
  type: '',
};
