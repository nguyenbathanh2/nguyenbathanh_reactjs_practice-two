import Textarea from './Textarea';
import styles from './textarea.module.css';

export default {
  title: 'Components/Textarea',
  component: Textarea,
};

function Template(args) {
  return <Textarea {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  classNameLabel: styles.label,
  placeholder: 'description...',
  className: styles.input_description,
};
