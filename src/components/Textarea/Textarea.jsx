// Libraries
import PropTypes from 'prop-types';
import React from 'react';
import './textarea.module.css';

const Textarea = ({
  label,
  onChange,
  value,
  type,
  classNameLabel,
  className,
  placeholder,
}) => {
  return (
    <label className={classNameLabel}>
      {label}
      <textarea
        onChange={onChange}
        value={value}
        className={className}
        placeholder={placeholder}
        type={type}
      />
    </label>
  );
};

Textarea.propTypes = {
  label: PropTypes.string,
  classNameLabel: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  className: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  type: PropTypes.string.isRequired,
};

Textarea.defaultProps = {
  label: null,
  classNameLabel: null,
  value: '',
  placeholder: null,
};

export default Textarea;
