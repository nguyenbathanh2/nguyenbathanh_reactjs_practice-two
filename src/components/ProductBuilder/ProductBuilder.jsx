/* eslint-disable react/forbid-prop-types */
// Libraries
import PropTypes from 'prop-types';
import React from 'react';

// Components
import ProductItem from '@components/ProductItem/ProductItem';

// Style
import styles from './productBuilder.module.css';

// eslint-disable-next-line react/function-component-definition
const ProductBuilder = ({ keyFilter, showPopup, allProductItems }) => {
  const getListFilter = () => {
    if (keyFilter === 'all') return allProductItems;
    else return allProductItems?.filter((item) => item.category === keyFilter);
  };
  return (
    <section className={styles.builder}>
      {getListFilter()?.length ? (
        getListFilter()?.map((productData) => (
          <ProductItem
            key={productData.id}
            image={productData.image}
            title={productData.title}
            desc={productData.desc}
            price={productData.price}
            id={productData.id}
            showPopup={showPopup}
          />
        ))
      ) : (
        <div className={styles.product_discovered}>
          There were no products discovered.
        </div>
      )}
    </section>
  );
};

ProductBuilder.propTypes = {
  keyFilter: PropTypes.string.isRequired,
  showPopup: PropTypes.func.isRequired,
  allProductItems: PropTypes.array.isRequired,
};

export default ProductBuilder;
