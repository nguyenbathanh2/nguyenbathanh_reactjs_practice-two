/* eslint-disable react/no-array-index-key */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable jsx-a11y/label-has-associated-control */
// Libraries
import PropTypes from 'prop-types';
import React from 'react';
import './select.module.css';

function Select({
  classNameLabel,
  classNameSelect,
  classNameOption,
  text,
  value,
  onChange,
  options,
}) {
  return (
    <label className={classNameLabel}>
      {text}
      <select className={classNameSelect} onChange={onChange} value={value}>
        {options?.map((option, data) => (
          <option key={data} value={option.value} className={classNameOption}>
            {option.label}
          </option>
        ))}
      </select>
    </label>
  );
}

Select.propTypes = {
  text: PropTypes.string,
  classNameLabel: PropTypes.string,
  classNameSelect: PropTypes.string,
  classNameOption: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  options: PropTypes.array.isRequired,
};

Select.defaultProps = {
  text: null,
  classNameLabel: null,
  classNameSelect: null,
  classNameOption: null,
  value: '',
};

export default Select;
