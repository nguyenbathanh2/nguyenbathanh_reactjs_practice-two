import Select from './Select';
import styles from './select.module.css';

export default {
  title: 'Components/Select',
  component: Select,
  argTypes: {
    options: [
      { value: '', label: 'Categories' },
      {
        value: 'breakfast',
        label: 'Breakfast',
      },
      {
        value: 'lunch',
        label: 'Lunch',
      },
      {
        value: 'shakes',
        label: 'Shakes',
      },
      {
        value: 'dinner',
        label: 'Dinner',
      },
    ],
  },
};

function Template(args) {
  return <Select {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  classNameLabel: styles.label,
  classNameSelect: styles.input_items,
  options: [
    { value: '', label: 'Categories' },
    {
      value: 'breakfast',
      label: 'Breakfast',
    },
    {
      value: 'lunch',
      label: 'Lunch',
    },
    {
      value: 'shakes',
      label: 'Shakes',
    },
    {
      value: 'dinner',
      label: 'Dinner',
    },
  ],
};
