/* eslint-disable react/react-in-jsx-scope */
/* eslint-disable react/jsx-props-no-spreading */
import SearchBar from './SearchBar';
import './searchBar.module.css';

export default {
  title: 'Components/SearchBar',
  component: SearchBar,
};

function Template(args) {
  return <SearchBar {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  search: () => {},
};
