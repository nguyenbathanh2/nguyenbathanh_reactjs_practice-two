// Libraries
import PropTypes from 'prop-types';
import React, { useState } from 'react';

// Components
import Input from '../Input/Input';

// Style
import styles from './searchBar.module.css';

function SearchBar({ search }) {
  const [searchInput, setSearchInput] = useState('');

  // Function to handle changes in the search input
  const handleSearchInputChange = (e) => {
    setSearchInput(e.target.value);
    search(e.target.value);
  };

  return (
    <Input
      className={styles.search}
      type="search"
      name="search"
      value={searchInput}
      placeholder="search.."
      onChange={handleSearchInputChange}
    />
  );
}

SearchBar.propTypes = {
  search: PropTypes.func.isRequired,
};

export default SearchBar;
