/* eslint-disable react/function-component-definition */
/* eslint-disable react/no-array-index-key */
/* eslint-disable react/no-children-prop */
// Libraries
import PropTypes from 'prop-types';
import React from 'react';

// Components
import Button from '@components/Button/Button';

//style
import styles from './categories.module.css';

const LIST_FILTER = ['all', 'breakfast', 'lunch', 'shakes', 'dinner'];

const Categories = ({ filter }) => {
  const handleClick = (key) => {
    filter(key);
  };
  return (
    <section className={styles.container}>
      {LIST_FILTER.map((option, id) => (
        <Button
          key={id}
          type="button"
          className={styles.filter_btn}
          children={option}
          onClick={() => handleClick(option)}
        />
      ))}
    </section>
  );
};
Categories.propTypes = {
  filter: PropTypes.func.isRequired,
};

export default Categories;
