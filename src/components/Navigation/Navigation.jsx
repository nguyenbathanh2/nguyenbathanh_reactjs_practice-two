// Libraries
import PropTypes from 'prop-types';
import React, { memo } from 'react';

// Style
import styles from './navigation.module.css';

const Navigation = ({ title }) => {
  return (
    <nav className={styles.wrapper}>
      <h2 className={styles.title_content}>{title}</h2>
      <hr className={styles.hr}></hr>
    </nav>
  );
};

Navigation.propTypes = {
  title: PropTypes.string.isRequired,
};

export default memo(Navigation);
