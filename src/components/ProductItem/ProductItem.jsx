/* eslint-disable react/prop-types */
/* eslint-disable react/no-children-prop */
/* eslint-disable react/jsx-curly-brace-presence */
// Libraries
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router-dom';

// Components
import Button from '@components/Button/Button';

// Style
import styles from './productItem.module.css';

function ProductItem({ id, title, image, desc, price, showPopup }) {
  // Function to handle when the user clicks on Delete button
  const handleClickDeleteItem = () => {
    showPopup(id);
  };
  return (
    <div className={styles.content_wrapper} id={`product-${id}`}>
      <img className={styles.card_image} src={image} />
      <div className={styles.detail_card}>
        <div className={styles.title_card}>
          <h4 className={styles.title_item}>{title}</h4>
          <h4 className={styles.price_item}>${price}</h4>
        </div>
        <p className={styles.desc_item}>{desc}</p>
        <div className={styles.button_card}>
          <Link to={`edit/${id}`}>
            <Button
              className={styles.button_edit}
              children={
                <FontAwesomeIcon icon={'fa-regular fa-pen-to-square'} />
              }
            />
          </Link>
          <Button
            className={styles.button_delete}
            onClick={handleClickDeleteItem}
            children={<FontAwesomeIcon icon={'fa-solid fa-trash'} />}
          />
        </div>
      </div>
    </div>
  );
}

ProductItem.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string,
  image: PropTypes.string,
  desc: PropTypes.string,
  showPopup: PropTypes.func.isRequired,
};

ProductItem.defaultProps = {
  title: '',
  image: '',
  desc: '',
};
export default ProductItem;
