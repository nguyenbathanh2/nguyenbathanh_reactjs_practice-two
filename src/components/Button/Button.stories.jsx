/* eslint-disable react/jsx-props-no-spreading */
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import Button from './Button';

// Style
import styles from './button.module.css';

library.add(fas, far);
export default {
  title: 'Components/Button',
  component: Button,
};

function Template(args) {
  return <Button {...args} />;
}

export const AddButton = Template.bind({});
AddButton.args = {
  onClick: () => {},
  className: styles.button_add,
  children: '+ Add a new dish',
};

export const ConfirmButton = Template.bind({});
ConfirmButton.args = {
  onclick: () => {},
  className: styles.confirm_alert_button_confirm,
  children: 'Create',
};

export const UpdateButton = Template.bind({});
UpdateButton.args = {
  onclick: () => {},
  className: styles.button_submit,
  children: 'Save',
};

export const CancelButton = Template.bind({});
CancelButton.args = {
  onClick: () => {},
  className: styles.confirm_alert_button_cancel,
  children: 'Cancel',
};

export const IconEditButton = Template.bind({});
IconEditButton.args = {
  onClick: () => {},
  className: styles.button_edit,
  children: <FontAwesomeIcon icon={'fa-regular fa-pen-to-square'} />,
};

export const IconDeleteButton = Template.bind({});
IconDeleteButton.args = {
  onclick: () => {},
  className: styles.button_delete,
  children: <FontAwesomeIcon icon={'fa-solid fa-trash'} />,
};
