// Libraries
import PropTypes from 'prop-types';
import React from 'react';

// Style
import './button.module.css';

// function button component
const Button = ({ onClick, children, className }) => {
  return (
    <button onClick={onClick} className={className}>
      {children}
    </button>
  );
};

Button.propTypes = {
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  children: PropTypes.node.isRequired,
};

Button.defaultProps = {
  onClick: () => {},
};

export default Button;
