// Libraries
import PropTypes from 'prop-types';
import React from 'react';

// Components
import Button from '../Button/Button';

// Style
import styles from './confirmPopup.module.css';

const ConfirmPopup = ({
  onCancel,
  onConfirm,
  openConfirmPopup,
  titleConfirm,
  messageConfirm,
}) => {
  return (
    openConfirmPopup && (
      <div className={styles.confirm_alert_overlay}>
        <div className={styles.confirm_alert}>
          <Button
            className={styles.close_button}
            onClick={onCancel}
            children="X"
          />
          <div className={styles.confirm_alert_body}>
            <span className={styles.title_confirm}>{titleConfirm}</span>
            <span className={styles.message_confirm}>{messageConfirm}</span>
            <div className={styles.confirm_alert_button_group}>
              <Button
                onClick={onConfirm}
                className={styles.confirm_alert_button_confirm}
                children="CONFIRM"
              />
              <Button
                onClick={onCancel}
                className={styles.confirm_alert_button_cancel}
                children="CANCEL"
              />
            </div>
          </div>
        </div>
      </div>
    )
  );
};

ConfirmPopup.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  openConfirmPopup: PropTypes.bool.isRequired,
  titleConfirm: PropTypes.string.isRequired,
  messageConfirm: PropTypes.string.isRequired,
};

export default ConfirmPopup;
