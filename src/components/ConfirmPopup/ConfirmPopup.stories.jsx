import ConfirmPopup from './ConfirmPopup';
import './confirmPopup.module.css';

export default {
  title: 'Components/ConfirmPopup',
  component: ConfirmPopup,
};

function Template(args) {
  return <ConfirmPopup {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  onCancel: () => {},
  onConfirm: () => {},
  openConfirmPopup: false,
  titleConfirm: 'CONFIRM',
  messageConfirm: 'Are you sure you want to delete this product ?',
};
