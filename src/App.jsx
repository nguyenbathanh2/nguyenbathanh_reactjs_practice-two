// Libraries
import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

//Pages
import AddProduct from '@pages/AddProduct/AddProduct';
import EditProduct from '@pages/EditProduct/EditProduct';
import HomePage from '@pages/HomePage/HomePage';

import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

// Style
import './App.css';

library.add(fas, far);

function App() {
  return (
    <React.Fragment>
      <Routes>
        <Route path={'/'} element={<Navigate to={'home'} />} />
        <Route path={'home'} element={<HomePage />} />
        <Route path={'home/add'} element={<AddProduct />} />
        <Route path={'home/edit/:id'} element={<EditProduct />} />
      </Routes>
    </React.Fragment>
  );
}

export default App;
