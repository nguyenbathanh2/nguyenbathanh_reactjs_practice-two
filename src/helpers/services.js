//Libraries
import axios from 'axios';

//constants
import { API_MENU, BASE_URL } from '@constants/api/api';

// Get data by GET method
export const getAllProduct = async () => {
  try {
    const res = await axios.get(`${BASE_URL}/${API_MENU}`);
    return res.data;
  } catch (error) {
    window.alert(`An error has occurred: ${error}`);
  }
};

// Get single data by GET method
export const getItem = async (id) => {
  try {
    const res = await axios.get(`${BASE_URL}/${API_MENU}/${id}`);
    return res.data;
  } catch (error) {
    window.alert(`An error has occurred: ${error}`);
  }
};

// Post data by POST method
export const postNewProduct = async (data) => {
  try {
    const res = await axios.post(`${BASE_URL}/${API_MENU}`, data);
    return res.data;
  } catch (error) {
    window.alert(`An error has occurred: ${error}`);
  }
};

// Update data by PUT method
export const editProduct = async (data) => {
  try {
    const { id, ...newData } = data;
    const res = await axios.put(`${BASE_URL}/${API_MENU}/${id}`, newData);
    return res.data;
  } catch (error) {
    window.alert(`An error has occurred: ${error}`);
  }
};

// Delete data by DELETE method
export const deleteProduct = async (id) => {
  try {
    const res = await axios.delete(`${BASE_URL}/${API_MENU}/${id}`);
    return res.data;
  } catch (error) {
    window.alert(`An error has occurred: ${error}`);
  }
};
