/* eslint-disable react/no-children-prop */
//Libraries
import React, { useContext, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

//actions
import { deleteItem, setAllProductItems } from '@store/actions/product.actions';

// Components
import Button from '@components/Button/Button';
import Categories from '@components/Categories/Categories';
import ConfirmPopup from '@components/ConfirmPopup/ConfirmPopup';
import Navigation from '@components/Navigation/Navigation';
import ProductBuilder from '@components/ProductBuilder/ProductBuilder';
import SearchBar from '@components/SearchBar/SearchBar';

//helpers
import { deleteProduct, getAllProduct } from '@helpers/services';

//Store
import { Context } from '@store/Context';

// Style
import styles from './homePage.module.css';

function HomePage() {
  const { state, dispatch } = useContext(Context);
  const [showPopup, setShowPopup] = useState(false);
  const [seletedItem, setSeletedItem] = useState('');
  const [error, setError] = useState('');
  const [keyFilter, setKeyFilter] = useState('all');
  const [itemShow, setItemShow] = useState([]);

  //Start loading the page, then call the API to get the data and assign it to the state
  const getProductList = async () => {
    try {
      const data = await getAllProduct();
      dispatch(setAllProductItems(data));
      setItemShow(data);
    } catch (e) {
      setError(e.message);
    }
  };

  // Function to handle delete the product
  const handleDelete = async () => {
    if (seletedItem) {
      const data = await deleteProduct(seletedItem);
      if (data) {
        dispatch(deleteItem(seletedItem));
      }
    }
    setShowPopup(false);
    const productId = document.getElementById(`product-${seletedItem}`);
    productId.remove();
  };

  // function to handle show popup
  const handleShowPopup = (id) => {
    setShowPopup(true);
    setSeletedItem(id);
  };

  // Function to handle search the product
  const handleSearch = async (searchInput) => {
    if (searchInput) {
      let data = [...state.allProductItems];
      data = data.filter((el) =>
        el.title.toLowerCase().includes(searchInput.trim().toLowerCase())
      );
      setItemShow(data);
    } else {
      getProductList();
    }
  };

  useEffect(() => {
    getProductList();
  }, []);

  return (
    <header className={styles.section}>
      <Navigation title="our menu" />
      <article className={styles.content}>
        <Categories filter={setKeyFilter} />
        <section className={styles.features}>
          <Link to="add">
            <Button
              id="button-add"
              className={styles.button_add}
              children="+ Add a new dish"
            />
          </Link>
          <SearchBar search={handleSearch} />
        </section>
        {!!error && <span className="error-api"> {error} </span>}
        {!error && (
          <ProductBuilder
            keyFilter={keyFilter}
            allProductItems={itemShow}
            showPopup={handleShowPopup}
          />
        )}
        <ConfirmPopup
          openConfirmPopup={showPopup}
          titleConfirm="CONFIRM"
          messageConfirm="Are you sure you want to delete this product ?"
          onConfirm={handleDelete}
          onCancel={() => setShowPopup(false)}
          handleClose={() => setShowPopup(false)}
        />
      </article>
    </header>
  );
}
export default HomePage;
