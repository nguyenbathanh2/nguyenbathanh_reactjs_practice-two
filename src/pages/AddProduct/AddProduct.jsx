// Libraries
import React, { useContext, useRef, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import validator from 'validator';

// data
import { addItem } from '@store/actions/product.actions.js';

// Components
import Button from '@components/Button/Button';
import Input from '@components/Input/Input';
import Navigation from '@components/Navigation/Navigation';
import Select from '@components/Select/Select';
import Textarea from '@components/Textarea/Textarea';

//store
import { Context } from '@store/Context';

//helpers
import { postNewProduct } from '@helpers/services';

//Style
import styles from './addProduct.module.css';

const AddProduct = () => {
  const titleRef = useRef(null);
  const navigation = useNavigate();
  const { dispatch } = useContext(Context);
  const [error, setError] = useState('');
  const [title, setTitle] = useState('');
  const [image, setImage] = useState('');
  const [price, setPrice] = useState('');
  const [desc, setDesc] = useState('');
  const [category, setCategory] = useState('');
  const [isValid, setIsValid] = useState('');

  // Function to handle input title
  const handleChangeTitle = (e) => {
    setTitle(e.target.value);
  };

  // function to handle input url image
  const handleChangeImage = (e) => {
    if (validator.isURL(e.target.value)) {
      setImage(e.target.value);
      setIsValid('is valid url');
    } else {
      setIsValid('is not valid url !!');
    }
  };

  // Function to handle input price
  const handleChangePrice = (e) => {
    setPrice(e.target.value);
  };

  // Function to handle input category
  const handleChangeCategory = (e) => {
    setCategory(e.target.value);
  };

  // Function to handle input description
  const handleChangeDesc = (e) => {
    setDesc(e.target.value);
  };

  // Function to handle submit and validation errors
  const handleClickSubmitForm = async () => {
    // If there is enough information, create a new item
    const newItem = { title, image, price, desc, category };
    if (title && image && price && desc && category) {
      const data = await postNewProduct(newItem);
      if (data) {
        dispatch(addItem(data));
        navigation('/home');
      }
      setIsValid();
    } else {
      setError('Please check and enter the full information !!');
      titleRef.current.focus();
    }
    resetForm();
  };

  //reset form
  const resetForm = () => {
    setTitle('');
    setImage('');
    setCategory('');
    setPrice('');
    setDesc('');
    setIsValid('');
  };

  return (
    <section className={styles.contact_add}>
      <Navigation title="add new a product" />
      <div className={styles.form_add}>
        <div className={styles.input_field}>
          <Input
            refInput={titleRef}
            classNameLabel={styles.label}
            label="The product name"
            className={styles.input_items}
            type="title"
            name="title"
            value={title}
            placeholder="title"
            onChange={handleChangeTitle}
          />
          <Input
            classNameLabel={styles.label}
            label="The product price"
            className={styles.input_items}
            type="number"
            name="price"
            value={price}
            minValue="1"
            placeholder="price"
            onChange={handleChangePrice}
          />
        </div>
        <div className={styles.input_field}>
          <div className={styles.error}>
            <Input
              classNameLabel={styles.label}
              label="The product image"
              className={styles.input_items}
              type="URL"
              name="image"
              value={image}
              placeholder="URL Image"
              onChange={handleChangeImage}
            />
            <span className={styles.data_fields_empty}>{isValid}</span>
          </div>
          <Select
            classNameLabel={styles.label}
            text="The product description"
            classNameSelect={styles.input_items}
            value={category}
            onChange={handleChangeCategory}
            options={[
              { value: '', label: 'Categories' },
              {
                value: 'breakfast',
                label: 'Breakfast',
              },
              {
                value: 'lunch',
                label: 'Lunch',
              },
              {
                value: 'shakes',
                label: 'Shakes',
              },
              {
                value: 'dinner',
                label: 'Dinner',
              },
            ]}
          />
        </div>
        <div className={styles.input_field}>
          <Textarea
            classNameLabel={styles.label}
            label="The product description"
            className={styles.input_description}
            type="description"
            name="description"
            value={desc}
            placeholder="description..."
            onChange={handleChangeDesc}
          />
        </div>
        <p className={styles.data_fields_empty}>{error}</p>
        <div className={styles.card_button}>
          <Button
            className={styles.button_submit}
            value="submit"
            type="submit"
            children="Create"
            onClick={handleClickSubmitForm}
          />
          <Link to={'/home'}>
            <Button
              value="cancel"
              className={styles.button_cancel}
              children="Cancel"
            />
          </Link>
        </div>
      </div>
    </section>
  );
};
export default AddProduct;
