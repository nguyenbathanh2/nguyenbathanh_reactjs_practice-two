// Libraries
import { Context } from '@store/Context';
import React, { useContext, useEffect, useState } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import validator from 'validator';

// data
import { editItem } from '@store/actions/product.actions.js';

//components
import Button from '@components/Button/Button';
import Input from '@components/Input/Input';
import Navigation from '@components/Navigation/Navigation';
import Select from '@components/Select/Select';
import Textarea from '@components/Textarea/Textarea';

//store
import { editProduct, getItem } from '@helpers/services';

// styles
import styles from './editProduct.module.css';

const EditProduct = () => {
  const navigation = useNavigate();
  const { state, dispatch } = useContext(Context);
  const [productSelected, setProductSelected] = useState({});
  const [error, setError] = useState('');
  const [valid, setValid] = useState('');
  const productId = useParams().id;

  const { allProductItems } = state || {};
  // get API
  useEffect(() => {
    async function getProductItem() {
      const data = await getItem(productId);
      setProductSelected(data);
    }
    getProductItem();
  }, [allProductItems]);

  // Function to handle input title
  const handleChangeTitle = (e) => {
    setProductSelected({ ...productSelected, title: e.target.value });
  };

  // function to handle input url image
  const handleChangeImage = (e) => {
    if (validator.isURL(e.target.value)) {
      setProductSelected({ ...productSelected, image: e.target.value });
      setValid('is valid url');
    } else {
      setValid('is not valid url');
    }
  };

  // Function to handle input price
  const handleChangePrice = (e) => {
    setProductSelected({
      ...productSelected,
      price: e.target.value,
    });
  };

  // Function to handle input category
  const handleChangeCategory = (e) => {
    setProductSelected({ ...productSelected, category: e.target.value });
  };

  // Function to handle input description
  const handleChangeDesc = (e) => {
    setProductSelected({ ...productSelected, desc: e.target.value });
  };

  // Function to handle submit and validation errors
  const handleUpdateForm = async () => {
    const { title, price, image, category, desc } = productSelected;
    if (title && image && price && desc && category) {
      const data = await editProduct(productSelected);
      if (data) {
        dispatch(editItem(data));
        navigation('/home');
      }
      setValid();
    } else {
      setError('please check and enter the full information !!');
    }
    resetForm();
  };

  //reset form
  const resetForm = () => {
    setValid('');
  };
  // Revert the validation state
  useEffect(() => {
    setError('');
  }, []);
  return (
    <section className={styles.contact}>
      <Navigation title="edit product" />
      <div className={styles.form_edit}>
        <div className={styles.input_field}>
          <Input
            classNameLabel={styles.label}
            label="the product name"
            className={styles.input_items}
            type="title"
            name="title"
            value={productSelected.title}
            placeholder="title"
            onChange={handleChangeTitle}
          />
          <Input
            classNameLabel={styles.label}
            label="the product price"
            className={styles.input_items}
            type="number"
            name="price"
            minValue="1"
            value={productSelected.price}
            placeholder="price"
            onChange={handleChangePrice}
          />
        </div>
        <div className={styles.input_field}>
          <div className={styles.error}>
            <Input
              classNameLabel={styles.label}
              label="the product image"
              className={styles.input_items}
              type="URL"
              name="image"
              value={productSelected.image}
              placeholder="URL Image"
              onChange={handleChangeImage}
            />
            <span className={styles.data_fields_empty}>{valid}</span>
          </div>
          <Select
            classNameLabel={styles.label}
            text="The product description"
            classNameSelect={styles.input_items}
            value={productSelected.category}
            onChange={handleChangeCategory}
            options={[
              { value: '', label: 'Categories' },
              {
                value: 'breakfast',
                label: 'Breakfast',
              },
              {
                value: 'lunch',
                label: 'Lunch',
              },
              {
                value: 'shakes',
                label: 'Shakes',
              },
              {
                value: 'dinner',
                label: 'Dinner',
              },
            ]}
          />
        </div>
        <Textarea
          classNameLabel={styles.label}
          label="the product description"
          className={styles.input_description}
          type="description"
          name="description"
          value={productSelected.desc}
          placeholder="description..."
          onChange={handleChangeDesc}
        />
        <p className={styles.data_fields_empty}>{error}</p>
        <div className={styles.card_button}>
          <Button
            className={styles.button_update}
            type="submit"
            children="Save"
            onClick={handleUpdateForm}
          />
          <Link to={'/home'}>
            <Button className={styles.button_cancel} children="Cancel" />
          </Link>
        </div>
      </div>
    </section>
  );
};

export default EditProduct;
