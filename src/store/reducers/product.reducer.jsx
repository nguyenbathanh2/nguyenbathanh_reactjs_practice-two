// constants
import {
  ADD_ITEM,
  DELETE_ITEM,
  EDIT_ITEM,
  SET_ALL_ITEMS,
} from '@constants/constants';

export const initialState = {
  allProductItems: [], // All products
};

export default function reducer(state, action) {
  switch (action.type) {
    case SET_ALL_ITEMS:
      return {
        allProductItems: action.allProductItems,
      };
    case ADD_ITEM:
      return {
        allProductItems: [...state.allProductItems, action.newItem],
      };
    case EDIT_ITEM:
      return {
        ...state,
        allProductItems: state.allProductItems?.map((item) => {
          if (item?.id === action.product?.id) return action.product;
          return item;
        }),
      };
    case DELETE_ITEM:
      const deleteProduct = state.allProductItems.filter(
        (item) => item.id !== action.id
      );
      return {
        ...state,
        allProductItems: deleteProduct,
      };
    default:
      throw new Error('Invalid action');
  }
}
