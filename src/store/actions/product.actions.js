// constants
import {
  ADD_ITEM,
  DELETE_ITEM,
  EDIT_ITEM,
  SET_ALL_ITEMS,
} from '@constants/constants';

export const setAllProductItems = (allProductItems) => {
  return {
    type: SET_ALL_ITEMS,
    allProductItems,
  };
};

export const addItem = (newItem) => {
  return {
    type: ADD_ITEM,
    newItem,
  };
};

export const editItem = (newItem, id) => {
  return {
    type: EDIT_ITEM,
    newItem,
    id,
  };
};

export const deleteItem = (id) => {
  return {
    type: DELETE_ITEM,
    id,
  };
};
