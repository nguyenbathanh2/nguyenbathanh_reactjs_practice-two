# ReactJS Practice Two

## Overview

- This document provides information about ReactJS Practice Two.

## Design

- [Here](https://docs.google.com/document/d/1ER4j-2ogNF7COnVbcM83dsGJZKTCyQLx/edit)

## Technical

- HTML5
- Module CSS
- [Node](https://nodejs.org/en/) (Ver 16.14.2)
- [ReactJS](https://www.reactjs.org/) (Ver 18.2.0)
- [React Router](https://reactrouter.com) (Ver ^6.4.2)
- [MockAPI](https://mockapi.io/)
- [Axios](https://axios-http.com) (Ver ^1.1.3)
- [Storybook](https://storybook.js.org/) (Ver 6.5)

## Timeline

- 11 days ( 2022/10/17 – 2022/10/31 )

## Teamsize

- 1 dev

## Targets

- Build an application for managing items using React (Ver 18.2.0)
- Understand & apply knowledge of HTML5, Module CSS, Storybook, ReactJS
- Understanding & applying React Hooks, React Context, React Router, React Memo
- Understand the importance of debug tools for front-end web developers (React DevTools, React perf,...)

## REQUIREMENTS

- Build the application with the features below:
- Create mocking data:
  - Render the list of products.
- Add the product:
  - The user can add a new product - Open the modal form to add a new one.
- Edit the product:
  - The user can edit the product when clicking the Edit button – Open the modal form to edit.
- Delete the product in the list.
  - The user can delete the product when clicking the Delete button – Show the popup confirmation.
- Search the product by name:
  - The user can search for the product by inputting the name of the product into the Search Bar.
- Sort the product:
  - The user can sort products by category when clicking on the "Category" button.

## ESTIMATION DETAILS

- > [Here](https://docs.google.com/document/d/1ER4j-2ogNF7COnVbcM83dsGJZKTCyQLx/edit)

## How to run the project

_Step one:_ Clone the code folder from gitlab to your device

- Choose a path to save that file -> At that path open the command window
- Run command
  > git clone https://gitlab.com/nguyenbathanh2/nguyenbathanh_reactjs_practice-two.git

_Step two:_ Run project

- cd project

- Turn on Visual Studio Code. Open the folder you just cloned to your computer.

- Install the npm dependencies

  > npm install

- Run start project

  > npm start

  - Or

  > npm run dev

- Hold ctrl and click on the successfully created localhost link to view the website

- View the project on port: http://localhost:5173/

## How to run the Storybook

- Build the Storybook with this command

  > npm run build-storybook

- Start the Storybook with this command

  > npm run storybook

- View the Storybook on port: http://localhost:6006
