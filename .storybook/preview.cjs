import {
  initialize,
  mswDecorator
} from 'msw-storybook-addon';
import '../src/assets/abstracts/_variable.css';
import "../src/components/Button/button.module.css";
import '../src/components/ConfirmPopup/confirmPopup.module.css';
import '../src/components/SearchBar/searchBar.module.css';
import '../src/components/Select/select.module.css';
import '../src/components/Textarea/textarea.module.css';
import '../src/index.css';
import '../src/pages/AddProduct/addProduct.module.css';

initialize();
export const decorators = [mswDecorator];

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
};
